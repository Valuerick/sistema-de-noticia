<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="UTF-8">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>G1 - Notícias :: @yield('titulo')</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

</head>

<body>

    <header>
        <div class="cor-primaria barra-fixa">
            <div class="container">
                <div class="row">

                    <div class="col-md-3 col-2">
                        <img src="{{asset('img/logo.jpg')}}" alt="G1" width="60">

                        
                    </div>

                    <div class="col-md-9 col-9" id="form-busca">
                        <form action="#" method="get">
                            <div class="form-group">
                                <input type="text" placeholder="Pesquisar..." class="form-control">
                            </div>
                        </form>
                    </div>

                    <span class="btn-collapse-menu"><i class="fas fa-bars"></i></span>

                </div>
            </div>
        </div>

        <div class="cor-segundaria collapse-menu collapse-menu-hide">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <nav id="menu">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Tecnologia</a></li>
                                <li><a href="#">Esportes</a></li>
                                <li><a href="#">Cinema</a></li>
                                <li><a href="#">Contato</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>



    </header>

    <main class="mt-5">

        @yield('conteudo')

    </main>

    <footer class="mt-5">

        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>&copy; <?=date('Y')?>{{--  ou {{ date('Y') }} --}} - Todos os direitos reservados</p>
                </div>
            </div>
        </div>

    </footer>
        <script src="{{asset('js/app.js')}}"></script>

</body>

</html>