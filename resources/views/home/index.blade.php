@extends('layouts.app')

@section('titulo','Home')

@section('conteudo')

<div class="container">

    <div class="row">
        <div class="col-md-4">
            <img class="img-fluid" src="https://via.placeholder.com/400x250">
        </div>
    

            <div class="col-md-7">
                <h2>Notícia Destaque</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime ducimus totam cumque tempora repellat ut molestiae impedit, labore earum iure nostrum doloribus aut cum autem natus. Nulla dicta at neque!</p>
            </div>
        </div>

    <div class="row">

        @for($i = 1; $i <= 3; $i++)

        <div class="col-md-4 mt-5">
                <article class="card">

                    <a href="#"><img class="img-fluid" src="https://via.placeholder.com/400x250"></a>

                    <div class="card-body">
                        <h2 class="card-title"><a href="#">Titulo Notícia</a></h2>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Facilis ut illo nobis provident omnis. Aliquid molestias nihil quia asperiores cum, et unde vitae incidunt quos temporibus saepe ad aspernatur voluptas.</p>
                    </div>

                    <div class="card-footer">
                        30/04/2019
                    </div>

                </article>
        </div>

        @endfor
        
    </div>

</div>


@endsection
